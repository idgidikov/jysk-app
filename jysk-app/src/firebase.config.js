// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import {getAuth} from 'firebase/auth';
import {getFirestore} from 'firebase/firestore';
import {getStorage} from 'firebase/storage';
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyA77tsJwybb_gTSbh7PSar2bWX-tXaDRdc",
  authDomain: "jusk-73df5.firebaseapp.com",
  projectId: "jusk-73df5",
  storageBucket: "jusk-73df5.appspot.com",
  messagingSenderId: "802835281587",
  appId: "1:802835281587:web:bcdbb6e5dd190e8b5de2c6"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const db = getFirestore(app);

export const storage = getStorage(app);
export default app;
export const auth = getAuth(app);
